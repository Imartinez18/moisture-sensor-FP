import RPi.GPIO as GPIO //
import time

channel = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.IN)

def callback(channel):
    if GPIO.input(channel):
        print('no moisture detected')
        f = open('save.txt','w')
        f.write('no moisture detected')
        f.close()
    else:
        print('moisture detected')
        f = open('save.txt','w')
        f.write('moisture detected')
        f.close()

GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)
GPIO.add_event_callback(channel, callback)

while True:
        time.sleep(1)
