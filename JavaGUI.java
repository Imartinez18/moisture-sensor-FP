import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.Integer;
import java.util.*;


class JavaGUI implements ActionListener{
	
	
	JFrame Gui = new JFrame("Moisture Stuffy");
	JPanel thePanel = new JPanel(new GridLayout(10,10));
	JTextArea outtie = new JTextArea(30,60);
	
	JButton show = new JButton("Results");
	JButton reset = new JButton("Reset");
	
	
	
	
	public JavaGUI() {
		
		Gui.setSize(300,300);
		
		outtie.setEditable(false);
		outtie.setPreferredSize(new Dimension (10,200));
		
	    show.addActionListener(this);
	    reset.addActionListener(this);
		
		
		thePanel.add(show);
		thePanel.add(outtie);
		thePanel.setBackground(new Color(205,192,176));
		thePanel.add(reset);	
		Gui.setContentPane(thePanel);
		Gui.setVisible(true);
		//Gui.pack();
		
	}
	//gotta have this bc of the implementation of ActionListener
	public void actionPerformed(ActionEvent event){
		
		if (event.getSource() == reset) {
				outtie.setText("");	
			 		
		}
		else if (event.getSource() == show){
			try {
				FileInputStream in = new FileInputStream("save.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String money;
			
				while((money = br.readLine()) !=null){
				
					outtie.setText(money);		
				}
			
			}
			catch(IOException ioe) {
			 		System.out.println("error");
			} 					 		 
			
		}
		
	}
	
	
	public static void main(String[] args){
		
		new JavaGUI();
		
	}	
	
	
}



	
